;; Pour la création de l'image Docker avec Guix
(specifications->manifest
 '(;; /bin/sh et mkdir sont requis par la CI de GitLab
   "bash-minimal"
   "coreutils"
   ;; Paquets indispensables pour l'export de org vers html
   "emacs-minimal"
   "emacs-org"
   "emacs-org-reveal"
   "emacs-htmlize"))
